import { Navigate, Outlet } from "react-router-dom";
import { useAppSelector } from "./redux/store";
import { getAccessTokenUser } from "./redux/auth/authSlice";

const PrivateRoute = () => {
  const isAuthenticated = useAppSelector(getAccessTokenUser);
  if (!isAuthenticated) return <Navigate to="/" replace />;
  return <Outlet />;
};

export default PrivateRoute;
