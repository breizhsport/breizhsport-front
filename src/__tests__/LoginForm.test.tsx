import { expect, describe, it } from "@jest/globals";
import { validateEmail, validatePassword } from "../helpers/validatorsHelper";

describe("validateEmail", () => {
  it("should return true for a valid email", () => {
    const validEmail = "test@example.com";
    expect(validateEmail(validEmail)).toBe(true);
  });

  it("should return false for an invalid email", () => {
    const invalidEmail = "invalid-email";
    expect(validateEmail(invalidEmail)).toBe(false);
  });
});

describe("validatePassword", () => {
  it("should return true for a password with at least 8 characters", () => {
    const validPassword = "password123";
    expect(validatePassword(validPassword)).toBe(true);
  });

  it("should return false for a password with less than 8 characters", () => {
    const invalidPassword = "pass";
    expect(validatePassword(invalidPassword)).toBe(false);
  });
});
