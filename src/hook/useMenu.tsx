import { Box, SxProps } from "@mui/material";
import React, { ReactElement, useEffect, useState } from "react";
import ReactDOM from "react-dom";

interface HookMenu {
  containerMenu: React.ReactPortal | null;
  closeMenu: () => void;
  showMenu: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

interface HookMenuProps {
  menuChild: ReactElement;
}

const useMenu = ({ menuChild }: HookMenuProps): HookMenu => {
  const [isOpen, setIsOpen] = useState(false);
  const [targetElement, setTargetElement] = useState<HTMLElement | null>(null);
  const [width, setWidth] = useState(0);
  const [points, setPoints] = useState({
    x: 0,
    y: 0,
  });

  const modalDivCss: SxProps = {
    zIndex: 1750,
    position: "fixed",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflow: "hidden",
  };

  const menuDivCss: SxProps = {
    top: `${points.y}px`,
    left: `${points.x}px`,

    position: "fixed",
  };

  const menu = (
    <Box sx={modalDivCss} onClick={() => setIsOpen(false)}>
      <Box sx={menuDivCss} onClick={(e) => e.stopPropagation()}>
        <Box
          sx={{
            backgroundColor: "#fff",
            width: `${width}px`,
          }}
        >
          {menuChild}
        </Box>
      </Box>
    </Box>
  );

  const showMenu = (e: React.ChangeEvent<HTMLInputElement>) => {
    let parentNode: HTMLElement = e.target as HTMLElement;
    let maxLevels = 10;
    for (let i = 0; i < maxLevels && !parentNode?.matches("input,div"); i++) {
      parentNode = parentNode?.parentElement!;
    }

    let rect = parentNode!.getBoundingClientRect();
    setPoints({
      x: rect.left,
      y: rect.top + rect.height,
    });
    setWidth(parentNode.clientWidth);
    setTargetElement(parentNode);
    setIsOpen(true);
  };

  const closeOnEscapeKey = (event: KeyboardEvent) => {
    if (event.key === "Escape") {
      setIsOpen(false);
    }
  };

  const updatePoints = () => {
    if (targetElement) {
      let rect = targetElement.getBoundingClientRect();

      setPoints({
        x: rect.left,
        y: rect.top + rect.height,
      });
      setWidth(targetElement.clientWidth);
    }
  };

  useEffect(() => {
    if (isOpen) {
      document.addEventListener("keydown", closeOnEscapeKey, false);
      window.addEventListener("resize", updatePoints, false);
    } else {
      document.removeEventListener("keydown", closeOnEscapeKey, false);
      window.removeEventListener("resize", updatePoints, false);
      setTargetElement(null);
    }
  }, [isOpen]);

  useEffect(() => {
    const pad = window.innerWidth - document.documentElement.clientWidth;
    document.body.style.overflow = isOpen ? "hidden" : "";
    document.body.style.paddingRight = isOpen ? `${pad}px` : "0px";
  }, [isOpen]);

  return {
    closeMenu: () => setIsOpen(false),
    containerMenu: isOpen ? ReactDOM.createPortal(menu, document.body) : null,
    showMenu,
  };
};
export default useMenu;
