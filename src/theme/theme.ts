import { createTheme, responsiveFontSizes } from "@mui/material/styles";

let theme = createTheme({
  palette: {
    primary: {
      main: "#00A1C1",
      contrastText: "#fff",
      light: "#E6F6F9",
    },
    secondary: {
      main: "#8DEB88",
      contrastText: "#000",
      light: "#E8FBE7",
    },
    success: { main: "#8DEB88" },
    background: {
      default: "#f8f9fa",
    },
    action: {
      hover: "#E6F6F9",
    },
  },
  typography: {
    fontFamily: "Comfortaa",
  },
  components: {
    MuiTextField: {
      styleOverrides: {
        root: {
          backgroundColor: "#fff",
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          borderRadius: 16,
        },
      },
    },
    MuiFormControl: {
      styleOverrides: {
        root: {
          borderRadius: 16,
        },
      },
    },
    MuiModal: {
      styleOverrides: {
        root: {
          borderRadius: 16,
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: 12,
        },
      },
    },
  },
});

theme = responsiveFontSizes(theme);

export default theme;
