import { Typography, Grid, Box, CircularProgress } from "@mui/material";
import { useLazyGetProductsListQuery } from "../redux/productService";
import { Product } from "../interfaces/ProductInterface";
import ProductCard from "../components/Product/ProductCard";
import { useParams } from "react-router-dom";
import { useEffect } from "react";

const ProductsPage = () => {
  const { category_id } = useParams();
  const [trigger, results] = useLazyGetProductsListQuery();
  const { data: productsList, isFetching, isError } = results;

  useEffect(() => {
    if (category_id) {
      trigger(Number.parseInt(category_id));
    }
  }, [category_id, trigger]);

  if (isFetching) {
    return <CircularProgress />;
  }

  if (isError) {
    return (
      <Typography>
        Erreur lors de la récupération de la catégorie : La catéogorie n'existe
        pas
      </Typography>
    );
  }

  return (
    <>
      <Box display={"inline"}>
        <Typography variant="h4">Liste des produits</Typography>
        <Typography variant="h6">
          {productsList && productsList.products.length > 0
            ? productsList.products.length +
              " produit" +
              (productsList.products.length > 1 ? "s " : " ") +
              "trouvé" +
              (productsList.products.length > 1 ? "s " : " ")
            : "Aucun produit trouvé"}
        </Typography>
      </Box>
      {isFetching ? (
        <CircularProgress size={40} sx={{ margin: "auto", marginTop: 4 }} />
      ) : (
        <Grid container spacing={2}>
          {productsList &&
            productsList.products.map((product: Product) => (
              <Grid item xs={12} sm={6} md={4} lg={3}>
                <ProductCard key={product.id} product={product} />
              </Grid>
            ))}
        </Grid>
      )}
    </>
  );
};

export default ProductsPage;
