import {
  Grid,
  Typography,
  useTheme,
  CircularProgress,
  Snackbar,
} from "@mui/material";
import HomePageCarousel from "../components/HomePage/HomePageCarousel";
import { useGetRecentProductsListQuery } from "../redux/productService";
import ProductCard from "../components/Product/ProductCard";
import { Product } from "../interfaces/ProductInterface";

const HomePage = () => {
  const theme = useTheme();
  const { data, isLoading, isError } = useGetRecentProductsListQuery(8);

  return (
    <>
      <HomePageCarousel />
      <Typography variant="h5">Dernières nouveautés</Typography>
      <Typography variant="body1" marginBottom={theme.spacing(3)}>
        Découvrez nos dernières pépites !
      </Typography>
      {isLoading ? (
        <CircularProgress size={40} sx={{ margin: "auto", marginTop: 4 }} />
      ) : (
        <Grid container spacing={2}>
          {isError ? (
            <Snackbar
              open={isError}
              message="Erreur lors du chargement des produits"
              autoHideDuration={6000}
            />
          ) : (
            data?.products &&
            data.products.map((product: Product) => (
              <Grid item xs={12} sm={6} md={4} lg={3} key={product.id}>
                <ProductCard product={product} />
              </Grid>
            ))
          )}
        </Grid>
      )}
    </>
  );
};

export default HomePage;
