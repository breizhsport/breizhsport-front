import { Typography, useTheme } from "@mui/material";
import PaymentForm from "../components/Payment/PaymentForm";
import { useAppSelector } from "../redux/store";
import { getCurrentUser } from "../redux/auth/authSlice";
import { useGetCartQuery } from "../redux/cartService";

const PaymentPage = () => {
  const theme = useTheme();
  const currentUser = useAppSelector(getCurrentUser);
  const { data } = useGetCartQuery(currentUser?.id);

  return (
    <>
      <Typography variant="h4" marginBottom={theme.spacing(3)}>
        Finalisation de votre commande
      </Typography>

      <PaymentForm cartId={data!.cart.id} />
    </>
  );
};

export default PaymentPage;
