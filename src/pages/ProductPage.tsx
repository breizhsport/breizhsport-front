import {
  Button,
  Card,
  CardContent,
  CircularProgress,
  Container,
  Typography,
  useTheme,
} from "@mui/material";
import { useParams } from "react-router-dom";
import { useAddProductCartMutation } from "../redux/cartService";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import { toast } from "react-toastify";
import { useLazyGetProductQuery } from "../redux/productService";
import { useEffect, useState } from "react";
import { useAppSelector } from "../redux/store";
import { getCurrentUser } from "../redux/auth/authSlice";
import UnauthorizedModal from "../components/UnauthorizedModal";

const ProductPage = () => {
  const theme = useTheme();
  const { product_id } = useParams();

  const currentUser = useAppSelector(getCurrentUser);

  const [trigger, result] = useLazyGetProductQuery();
  const { data: product, isError, isFetching } = result;
  const [addToCart, {}] = useAddProductCartMutation();

  const [dialogOpen, setDialogOpen] = useState(false);

  const handleClickAddProduct = async (productId: number) => {
    if (currentUser.id == null) {
      setDialogOpen(true); // Ouvrir la boîte de dialogue si currentUser est null
      return;
    }
    try {
      await addToCart({ productId: productId });
      toast.success("Produit ajouté au panier");
    } catch (error) {
      toast.error("Erreur lors de l'ajout au panier");
    }
  };

  const handleCloseDialog = () => {
    setDialogOpen(false); // Fermer la boîte de dialogue
  };

  useEffect(() => {
    if (product_id) {
      trigger(Number.parseInt(product_id));
    }
  }, [product_id, trigger]);

  if (isFetching) {
    return <CircularProgress />;
  }

  if (isError) {
    return (
      <Typography>
        Erreur lors de la récupération du produit : Le produit n'existe pas
      </Typography>
    );
  }

  return (
    <Container sx={{ paddingTop: theme.spacing(4) }}>
      <Card elevation={3} sx={{ maxWidth: 600, margin: "auto" }}>
        <img
          alt="Image du produit"
          style={{ objectFit: "cover", width: "100%", height: 300 }}
          src={product?.product.image}
        />
        <CardContent>
          <Typography variant="h5" gutterBottom>
            {product?.product.title}
          </Typography>
          <Typography variant="body2" color="text.secondary" paragraph>
            {product?.product.description}
          </Typography>
          <Button
            onClick={() => handleClickAddProduct(product!.product.id)}
            variant="contained"
            color="secondary"
            startIcon={<AddShoppingCartIcon />}
            sx={{
              width: "100%",
              marginTop: theme.spacing(2),
            }}
          >
            Ajouter au panier
          </Button>
        </CardContent>
        <UnauthorizedModal
          dialogOpen={dialogOpen}
          handleCloseDialog={handleCloseDialog}
        />
      </Card>
    </Container>
  );
};

export default ProductPage;
