import React, { ReactNode, useEffect, useState } from "react";

interface ErrorBoundaryProps {
  children: ReactNode;
}

interface ErrorBoundaryState {
  hasError: boolean;
}

const ErrorBoundary: React.FC<ErrorBoundaryProps> = ({ children }) => {
  const [errorState, setErrorState] = useState<ErrorBoundaryState>({
    hasError: false,
  });

  useEffect(() => {
    const handleError = (errorEvent: ErrorEvent): void => {
      setErrorState({ hasError: true });
      // Vous pouvez également enregistrer l'erreur dans un service de journalisation
      const error = new Error(errorEvent.message);
      error.name = "Error"; // Ajouter le nom à l'objet Error
      console.error("Error caught by ErrorBoundary:", error);
    };

    window.addEventListener("error", handleError);

    return () => {
      window.removeEventListener("error", handleError);
    };
  }, []);

  if (errorState.hasError) {
    // Vous pouvez rendre un composant de remplacement en cas d'erreur
    return <div>Something went wrong.</div>;
  }

  return <>{children}</>;
};

export default ErrorBoundary;
