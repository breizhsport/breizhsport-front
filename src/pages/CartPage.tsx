import { CreditCard } from "@mui/icons-material";
import {
  Typography,
  Button,
  Grid,
  useTheme,
  Box,
  Alert,
  useMediaQuery,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useGetCartQuery } from "../redux/cartService";
import { useAppSelector } from "../redux/store";
import { getCurrentUser } from "../redux/auth/authSlice";
import ProductCartCard from "../components/Cart/ProductCartCard";
import CartSummaryCard from "../components/Cart/CartSummaryCard";

const CartPage = () => {
  const navigate = useNavigate();
  const theme = useTheme();

  const currentUser = useAppSelector(getCurrentUser);

  const isMd = useMediaQuery(theme.breakpoints.down("md"));

  const handleClick = () => {
    navigate("/payment");
  };

  const { data } = useGetCartQuery(currentUser?.id);

  if (data?.cart == null || data.cart.products.length === 0) {
    return (
      <>
        <Typography variant="h4"> Mon panier</Typography>
        <Alert severity="info">
          <Typography>Votre panier est vide</Typography>
        </Alert>
      </>
    );
  }

  return (
    <>
      <Typography variant="h4" marginBottom={theme.spacing(4)}>
        Mon panier
      </Typography>
      <Grid container spacing={theme.spacing(3)} justifyContent="space-between">
        <Grid
          item
          xs={12}
          md={6}
          style={{ maxHeight: "400px", overflowY: "auto" }}
        >
          {data && data.cart.products.length > 0 && (
            <>
              {data.cart.products.map((product) => (
                <ProductCartCard key={product.id} product={product} />
              ))}
            </>
          )}
        </Grid>
        {data && data.cart.products.length > 0 && (
          <Grid item xs={12} md={5}>
            <CartSummaryCard cart={data.cart} />
            <Box
              display="flex"
              justifyContent="end"
              marginTop={theme.spacing(2)}
            >
              <Button
                onClick={handleClick}
                variant="contained"
                startIcon={<CreditCard />}
              >
                Passer au paiement
              </Button>
            </Box>
          </Grid>
        )}
      </Grid>
    </>
  );
};

export default CartPage;
