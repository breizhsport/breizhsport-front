import { useTheme, Grid, Typography } from "@mui/material";
import OrdersListCard from "../components/Order/OrdersListCard";
import ProfilDetailsCard from "../components/Profil/ProfilDetailsCard";

const ProfilPage = () => {
  const theme = useTheme();

  return (
    <Grid container spacing={3}>
      {/* Pour les petits écrans, afficher ProfilDetailsCard sur une seule colonne */}
      <Grid item xs={12} sm={4}>
        <ProfilDetailsCard />
      </Grid>
      {/* Pour les petits écrans, afficher OrdersListCard sur une seule colonne */}
      <Grid item xs={12} sm={7}>
        <Typography marginBottom={theme.spacing(3)} variant="h4">
          Mon historique de commande
        </Typography>
        <OrdersListCard />
      </Grid>
    </Grid>
  );
};

export default ProfilPage;
