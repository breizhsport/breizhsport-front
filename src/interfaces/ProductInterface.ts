export interface Product {
  id: number;
  title: string;
  description: string;
  price: DoubleRange;
  categoryId: number;
  orderId: number;
  image: string;
}

export interface OrderProduct {
  id: number;
  title: string;
  price: number;
  quantity: number;
  cart_id: number;
  product_id: number;
  created_at: string;
}

export interface ProductCardProps {
  product: Product;
}
export interface ProductPageProps {
  product_id: number;
}

export interface ProductOrder extends Product {
  quantity: number;
}

export interface ProductResponse {
  product: Product;
}

export interface ProductListResponse {
  products: Product[];
}

export interface SuggestionsListProps {
  suggestList: ProductListResponse;
  query: string;
}
