import { OrderProduct } from "./ProductInterface";

export interface Cart {
  id: number;
  products: OrderProduct[];
  productsNumber: number;
  totalPrice: number;
  customer_id: number;
  created_at: string;
}

export interface GetCartResponse {
  cart: Cart;
}

export interface UpdateQuantityButtonProps {
  product: OrderProduct;
  setQuantity: React.Dispatch<React.SetStateAction<number>>;
  quantity: number;
}

export interface DeleteProductCartButtonProps {
  product: OrderProduct;
}

export interface ProductCartCardProps {
  product: OrderProduct;
}
export interface PaymentFormProps {
  cartId: number;
}

export interface CartSummaryProps {
  cart: Cart;
}

export interface AddProductCartInput {
  productId: number;
}

export interface UpdateQuantityInput {
  userId: number;
  productId: number;
  newQuantity: number;
  cartId: number | undefined;
}

export interface DeleteProductCartInput {
  productId: number;
}
