export interface ButtonModalParams {
  icon: React.ReactNode;
  children: React.ReactElement;
  label: string;
  id?: string;
  open: boolean;
  setOpen: (open: boolean) => void;
}
