export interface Category {
  id: number;
  title: String;
}

export interface CategoryButtonProps {
  onClick: () => void;
  category: Category;
  selected: boolean;
}

export interface CategoriesListResponse {
  categories: Category[];
}
