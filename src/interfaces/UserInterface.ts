export interface User {
  id: number;
  email: string;
  lastname: string;
  firstname: string;
  token: string;
}

export interface RegisterFormInput {
  lastname: string;
  firstname: string;
  password: string;
  email: string;
  phone: string;
  address: string;
  zipcode: string;
  city: string;
}

export interface LoginFormInput {
  email: string;
  password: string;
}

export interface LoginResponse {
  user: User;
}

export interface UserEditForm {
  userId: number;
  email: string;
  actualPassword: string;
  newPassword: string;
}
