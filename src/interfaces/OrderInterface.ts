import { ProductOrder } from "./ProductInterface";

export interface OrderCardParams {
  order: Order;
}

export interface Order {
  id: number;
  customerId: number;
  products: ProductOrder[];
  createdAt: Date;
  totalPrice: number;
  productsNumber: number;
}

export interface OrdersListResponse {
  orders: Order[];
}
