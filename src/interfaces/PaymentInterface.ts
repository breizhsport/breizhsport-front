export interface PaymentFormInput {
  creditCardName: String;
  creditCardNumber: String;
  validityDate: String;
  CVC: String;
}

export interface PaymentConfirmationModalParams {
  open: boolean;
  handleClose: () => void;
}
