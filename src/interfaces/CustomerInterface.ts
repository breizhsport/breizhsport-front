export interface Customer {
  id: number;
  lastname: string;
  firstname: string;
  address: string;
  city: string;
  zipcode: string;
  userId: number;
  email: string;
  phone: string;
}

export interface GetCustomerResponse {
  customer: Customer;
}
