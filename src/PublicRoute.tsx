import { Navigate } from "react-router-dom";
import { useAppSelector } from "./redux/store";
import { getAccessTokenUser } from "./redux/auth/authSlice";
import HomePage from "./pages/HomePage";

const PublicRoute = () => {
  const isAuthenticated = useAppSelector(getAccessTokenUser);
  if (isAuthenticated) return <Navigate to="/" replace />;
  return <HomePage />;
};

export default PublicRoute;
