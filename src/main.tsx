import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import "react-toastify/dist/ReactToastify.css";
import Layout from "./components/Layout";
import { ThemeProvider } from "@emotion/react";
import { Alert, createTheme } from "@mui/material";
import { Provider } from "react-redux";
import theme from "./theme/theme";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import HomePage from "./pages/HomePage";
import { store } from "./redux/store";
import ProductsPage from "./pages/ProductsPage";
import CartPage from "./pages/CartPage";
import PaymentPage from "./pages/PaymentPage";
import { ToastContainer } from "react-toastify";
import ProfilPage from "./pages/ProfilPage";
import ProductPage from "./pages/ProductPage";
import PrivateRoute from "./PrivateRoute";

const muiTheme = createTheme(theme);

const router = createBrowserRouter([
  {
    // Indique les routes accessible par tout le monde
    element: <Layout />,
    children: [
      { path: "/", element: <HomePage /> },
      { path: "/category/:category_id", element: <ProductsPage /> },
      { path: "/product/:product_id", element: <ProductPage /> },

      {
        element: <PrivateRoute />,
        children: [
          { path: "/cart", element: <CartPage /> },
          { path: "/payment", element: <PaymentPage /> },
          { path: "/profil", element: <ProfilPage /> },
        ],
      },
      {
        path: "*",
        element: <Alert color="error">Error : Page not found</Alert>,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <ThemeProvider theme={muiTheme}>
      <Provider store={store}>
        <ToastContainer position="bottom-right" theme="light" />
        <RouterProvider router={router} />
      </Provider>
    </ThemeProvider>
  </React.StrictMode>
);
