import { Grid, Box, useTheme, Typography, useMediaQuery } from "@mui/material";
import SocialNetworkLinks from "./SocialNetworkLinks";
import logo from "../../assets/logo_breizhsport_white.png";

const FooterBase = () => {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.up("md"));
  return (
    <Box
      sx={{
        backgroundColor: theme.palette.primary.main,
        padding: theme.spacing(3),
      }}
    >
      <Grid flexDirection={isMd ? "row" : "column"} container>
        <Grid item xs={12} md={2}>
          <img
            src={logo}
            alt="Logo"
            width={"50px"}
            style={{ minWidth: "30px", cursor: "pointer" }}
          />
        </Grid>
        <Grid item xs={12} md={3}>
          <Typography>C.G.U</Typography>
          <Typography>Politique de confidentialité</Typography>
        </Grid>
        <Grid item xs={12} md={3}>
          <Typography>Nous contacter</Typography>
          <Typography>A propos de BreizhSport</Typography>
        </Grid>
        <Grid item xs={12} md={3} sx={{ textAlign: "center" }}>
          <Typography>Retrouvez-nous sur les réseaux sociaux !</Typography>
          <SocialNetworkLinks />
        </Grid>
      </Grid>
    </Box>
  );
};

export default FooterBase;
