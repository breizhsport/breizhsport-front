import { Instagram, Pinterest, Twitter, YouTube } from "@mui/icons-material";
import { Box, useTheme } from "@mui/material";

const SocialNetworkLinks = () => {
  const theme = useTheme();

  return (
    <>
      <Box>
        <Instagram sx={{ marginRight: theme.spacing(2) }} />
        <YouTube sx={{ marginRight: theme.spacing(2) }} />
        <Twitter sx={{ marginRight: theme.spacing(2) }} />
        <Pinterest sx={{ marginRight: theme.spacing(2) }} />
      </Box>
    </>
  );
};

export default SocialNetworkLinks;
