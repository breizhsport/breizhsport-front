import {
  DialogTitle,
  Typography,
  Dialog,
  DialogContent,
  DialogActions,
  Button,
} from "@mui/material";

interface UnauthorizedModalProps {
  dialogOpen: boolean;
  handleCloseDialog: () => void;
}

const UnauthorizedModal = ({
  dialogOpen,
  handleCloseDialog,
}: UnauthorizedModalProps) => {
  return (
    <Dialog open={dialogOpen} onClose={handleCloseDialog}>
      <DialogTitle>Vous devez être connecté</DialogTitle>
      <DialogContent>
        <Typography>
          Vous devez être authentifié pour réaliser cette action !
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseDialog} color="primary">
          Fermer
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default UnauthorizedModal;
