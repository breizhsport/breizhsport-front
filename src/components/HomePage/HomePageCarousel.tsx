import { Box } from "@mui/material";
import Carousel from "react-material-ui-carousel";
import shirt1 from "../../assets/carousel/shirt1.png";
import shoes1 from "../../assets/carousel/shoes1.png";
import shoes2 from "../../assets/carousel/shoes2.png";

const HomePageCarousel = () => {
  interface CarouselItem {
    img: string;
    legend: string;
  }

  const items: CarouselItem[] = [
    { img: shirt1, legend: "Test 1" },
    { img: shoes1, legend: "Test 2" },
    { img: shoes2, legend: "Test 2" },
  ];

  return (
    <Box sx={{ width: "100%", overflow: "hidden" }}>
      <Carousel
        autoPlay={true}
        animation="slide"
        indicators={true}
        navButtonsAlwaysVisible={true}
        interval={5000}
      >
        {items.map((item, index) => (
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              width: "100%", // Ajout de cette propriété pour s'assurer que le conteneur du Carousel prend 100% de la largeur
            }}
            key={`carousel-image-${index}`}
          >
            <img
              src={item.img}
              alt={`Carousel image ${index}`}
              style={{ width: "100%", height: "auto", maxWidth: "100%" }} // Ajout de maxWidth pour assurer la réactivité
            />
          </Box>
        ))}
      </Carousel>
    </Box>
  );
};

export default HomePageCarousel;
