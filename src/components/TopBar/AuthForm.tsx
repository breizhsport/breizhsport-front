import { Button, FormControl, useTheme } from "@mui/material";
import { useState } from "react";
import SignInForm from "./SignInForm";
import LoginForm from "./LoginForm";

const AuthForm = ({ handleClose }: { handleClose: () => void }) => {
  const theme = useTheme();
  const [isLogin, setIsLogin] = useState(true);

  const handleToggle = () => {
    setIsLogin(!isLogin);
  };

  return (
    <FormControl fullWidth>
      {isLogin ? (
        <LoginForm handleClose={handleClose} />
      ) : (
        <SignInForm handleClose={handleClose} />
      )}
      <Button
        onClick={handleToggle}
        variant="outlined"
        sx={{ marginTop: theme.spacing(2) }}
      >
        {isLogin ? "Créer un compte" : "Se connecter"}
      </Button>
    </FormControl>
  );
};

export default AuthForm;
