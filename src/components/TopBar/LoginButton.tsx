import { AccountCircle } from "@mui/icons-material";

import { useState } from "react";
import AuthForm from "./AuthForm";
import ButtonModal from "../ButtonModal";

const LoginButton = () => {
  const [open, setOpen] = useState(false);
  const handleClose = () => setOpen(false);

  return (
    <ButtonModal
      id="login-button"
      icon={<AccountCircle />}
      label="Se connecter"
      open={open}
      setOpen={setOpen}
    >
      <AuthForm handleClose={handleClose} />
    </ButtonModal>
  );
};
export default LoginButton;
