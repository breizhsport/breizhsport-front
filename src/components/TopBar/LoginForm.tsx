import {
  Close,
  Lock,
  Login,
  Mail,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";
import {
  Box,
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import { useLoginMutation } from "../../redux/userService";
import { LoginFormInput } from "../../interfaces/UserInterface";
import { useForm } from "react-hook-form";
import {
  validateEmail,
  validatePassword,
} from "../../helpers/validatorsHelper";
import { toast } from "react-toastify";
import { useEffect, useState } from "react";
import { RequestError } from "../../interfaces/ErrorInterface";
import { isFetchError } from "../../helpers/requestHelper";

const LoginForm = ({ handleClose }: { handleClose: () => void }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginFormInput>({
    defaultValues: { email: "", password: "" },
  });

  const [login, { isSuccess, isError, error }] = useLoginMutation();

  useEffect(() => {
    if (!isError) {
      return;
    }

    if (isFetchError(error)) {
      const errorData = error.data as RequestError;
      toast.error(errorData.message);
    }
  }, [isError]);

  useEffect(() => {
    if (!isSuccess) {
      return;
    }
    toast.success("Connexion réussie !");
    handleClose();
  }, [isSuccess]);

  const onSubmit = (data: LoginFormInput) => {
    const formData = new FormData();
    formData.append("email", data.email);
    formData.append("password", data.password);
    login(formData);
  };

  const [showPassword, setShowPassword] = useState(false);

  const handleTogglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === "Enter") {
      handleSubmit(onSubmit)();
    }
  };

  return (
    <Box onKeyDown={handleKeyDown}>
      <FormControl fullWidth margin="none">
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Typography variant="h5">Se connecter</Typography>
          <IconButton onClick={handleClose}>
            <Close />
          </IconButton>
        </Box>
        <TextField
          id="email"
          label="Email"
          variant="outlined"
          size="small"
          margin="normal"
          InputProps={{ startAdornment: <Mail /> }}
          {...register("email", {
            required: { value: true, message: "Valeur manquante" },
            validate: (value) =>
              validateEmail(value) || "Format d'email invalide",
          })}
          error={!!errors.email}
          helperText={errors.email?.message}
        />
        <TextField
          label="Mot de passe"
          variant="outlined"
          size="small"
          margin="normal"
          InputProps={{
            startAdornment: <Lock />,
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={handleTogglePasswordVisibility} edge="end">
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
          type={showPassword ? "text" : "password"}
          {...register("password", {
            required: { value: true, message: "Valeur manquante" },
            validate: (value) =>
              validatePassword(value) ||
              "Le mot de passe doit avoir au moins 8 caractères",
          })}
          error={!!errors.password}
          helperText={errors.password?.message}
        />
        <Button
          id="login"
          variant="contained"
          color="primary"
          startIcon={<Login />}
          onClick={handleSubmit(onSubmit)}
          sx={{ mt: 2 }}
        >
          Se connecter
        </Button>
      </FormControl>
    </Box>
  );
};

export default LoginForm;
