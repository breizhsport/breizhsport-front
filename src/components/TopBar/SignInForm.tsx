import {
  Close,
  Login,
  Lock,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";
import {
  Box,
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import { RegisterFormInput } from "../../interfaces/UserInterface";
import { useForm } from "react-hook-form";
import { useSignInMutation } from "../../redux/userService";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import {
  validateEmail,
  validatePassword,
} from "../../helpers/validatorsHelper";
import { useState } from "react";

const SignInForm = ({ handleClose }: { handleClose: () => void }) => {
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<RegisterFormInput>({
    defaultValues: {
      lastname: "",
      firstname: "",
      email: "",
      password: "",
      phone: "",
      address: "",
      zipcode: "",
      city: "",
    },
  });

  const [signIn, {}] = useSignInMutation();
  const [showPassword, setShowPassword] = useState(false);

  const handleTogglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const onSubmit = async (data: RegisterFormInput) => {
    try {
      await signIn(data);
      toast.success("Le compte a été créé avec succès !");
      handleClose();
      navigate("/");
    } catch (error) {
      handleClose();
      toast.error("Erreur lors de la création du compte");
    }
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === "Enter") {
      handleSubmit(onSubmit)();
    }
  };

  return (
    <Box onKeyDown={handleKeyDown}>
      <FormControl fullWidth margin="none">
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Typography variant="h5">Créer un compte</Typography>
          <IconButton onClick={handleClose}>
            <Close />
          </IconButton>
        </Box>

        <TextField
          label="Prénom"
          variant="outlined"
          size="small"
          sx={{ width: "100%" }}
          margin="normal"
          {...register("lastname", {
            required: { value: true, message: "Valeur manquante" },
          })}
          error={!!errors.lastname}
          helperText={errors.lastname?.message}
        />

        <TextField
          label="Nom"
          variant="outlined"
          size="small"
          sx={{ width: "100%" }}
          margin="normal"
          {...register("firstname", {
            required: { value: true, message: "Valeur manquante" },
          })}
          error={!!errors.firstname}
          helperText={errors.firstname?.message}
        />

        <TextField
          id="outlined-basic"
          label="Email"
          variant="outlined"
          size="small"
          sx={{ width: "100%" }}
          margin="normal"
          {...register("email", {
            required: { value: true, message: "Valeur manquante" },
            validate: (value) =>
              validateEmail(value) || "Format d'email invalide",
          })}
          error={!!errors.email}
          helperText={errors.email?.message}
        />

        <TextField
          label="Mot de passe"
          variant="outlined"
          size="small"
          margin="normal"
          InputProps={{
            startAdornment: <Lock />,
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={handleTogglePasswordVisibility} edge="end">
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
          type={showPassword ? "text" : "password"}
          {...register("password", {
            required: { value: true, message: "Valeur manquante" },
            validate: (value) =>
              validatePassword(value) ||
              "Le mot de passe doit avoir au moins 8 caractères",
          })}
          error={!!errors.password}
          helperText={errors.password?.message}
        />

        <TextField
          id="outlined-basic"
          label="Numéro de téléphone"
          variant="outlined"
          size="small"
          sx={{ width: "100%" }}
          margin="normal"
          {...register("phone", {
            required: { value: true, message: "Valeur manquante" },
          })}
          error={!!errors.phone}
          helperText={errors.phone?.message}
        />

        <TextField
          id="outlined-basic"
          label="Adresse"
          variant="outlined"
          size="small"
          sx={{ width: "100%" }}
          margin="normal"
          {...register("address", {
            required: { value: true, message: "Valeur manquante" },
          })}
          error={!!errors.address}
          helperText={errors.address?.message}
        />

        <TextField
          id="outlined-basic"
          label="Code postal"
          variant="outlined"
          size="small"
          sx={{ width: "100%" }}
          margin="normal"
          {...register("zipcode", {
            required: { value: true, message: "Valeur manquante" },
          })}
          error={!!errors.zipcode}
          helperText={errors.zipcode?.message}
        />

        <TextField
          id="outlined-basic"
          label="Ville"
          variant="outlined"
          size="small"
          sx={{ width: "100%" }}
          margin="normal"
          {...register("city", {
            required: { value: true, message: "Valeur manquante" },
          })}
          error={!!errors.city}
          helperText={errors.city?.message}
        />

        <Button
          variant="contained"
          color="primary"
          startIcon={<Login />}
          sx={{ mt: 2 }}
          onClick={handleSubmit(onSubmit)}
        >
          Créer
        </Button>
      </FormControl>
    </Box>
  );
};

export default SignInForm;
