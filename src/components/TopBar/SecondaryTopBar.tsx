import { useState } from "react";
import {
  AppBar,
  Container,
  Toolbar,
  IconButton,
  Drawer,
  List,
  ListItem,
  ListItemText,
  useTheme,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { useGetCategoriesListQuery } from "../../redux/categoryService";
import CategoryButton from "./CategoryButton";
import { useLocation, useNavigate } from "react-router-dom";

const SecondaryTopBar = () => {
  const theme = useTheme();
  const { data: categoriesList } = useGetCategoriesListQuery();

  const location = useLocation();
  const { state } = location;
  const categoryId = state ? state.category_id : null;

  const navigate = useNavigate();

  const [drawerOpen, setDrawerOpen] = useState(false);

  const handleCategoryClick = (categoryId: number) => {
    navigate(`/category/${categoryId}`);
    setDrawerOpen(false);
  };

  return (
    <>
      <AppBar
        position="static"
        elevation={0}
        sx={{
          backgroundColor: theme.palette.primary.light,
        }}
      >
        <Container sx={{ textAlign: "center", alignItems: "center" }}>
          <Toolbar disableGutters sx={{ justifyContent: "space-between" }}>
            {/* Afficher l'icône du menu uniquement en mode mobile (sm) */}
            <IconButton
              color="primary"
              onClick={() => setDrawerOpen(true)}
              sx={{
                display: { sm: "block", md: "none" },
              }}
            >
              <MenuIcon />
            </IconButton>

            <Container sx={{ display: { xs: "none", md: "flex" } }}>
              {categoriesList &&
                categoriesList.categories.map((category) => (
                  <CategoryButton
                    key={category.id}
                    category={category}
                    onClick={() => handleCategoryClick(category.id)}
                    selected={category.id === categoryId}
                  />
                ))}
            </Container>
          </Toolbar>
        </Container>
      </AppBar>

      {/* Menu déroulant pour les tailles d'écran mobiles (sm) */}
      <Drawer
        anchor="left"
        open={drawerOpen}
        onClose={() => setDrawerOpen(false)}
      >
        <List>
          {categoriesList &&
            categoriesList.categories.map((category) => (
              <ListItem
                button
                key={category.id}
                selected={category.id === categoryId}
                onClick={() => handleCategoryClick(category.id)}
              >
                <ListItemText primary={category.title} />
              </ListItem>
            ))}
        </List>
      </Drawer>
    </>
  );
};

export default SecondaryTopBar;
