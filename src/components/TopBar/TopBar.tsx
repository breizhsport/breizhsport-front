import { Search } from "@mui/icons-material";
import logo from "../../assets/logo_breizhsport_white.png";
import {
  Container,
  InputAdornment,
  TextField,
  AppBar,
  Box,
  Toolbar,
  useTheme,
  useMediaQuery,
  List,
  ListItem,
  Typography,
} from "@mui/material";
import LoginButton from "./LoginButton";
import CartButton from "./CartButton";
import { getAccessTokenUser } from "../../redux/auth/authSlice";
import { useAppSelector } from "../../redux/store";
import AccountButton from "./AccountButton";
import SecondaryTopBar from "./SecondaryTopBar";
import { useNavigate } from "react-router-dom";
import { useGetSuggestionsQuery } from "../../redux/productService";
import { useEffect, useState } from "react";
import useMenu from "../../hook/useMenu";

function TopBar() {
  const isAuthenticated = useAppSelector(getAccessTokenUser);
  const navigate = useNavigate();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  const [query, setQuery] = useState("");

  const handleClick = () => {
    navigate(`/`);
  };

  const { data: suggestList, refetch: refetchSuggestions } =
    useGetSuggestionsQuery(query);

  useEffect(() => {
    refetchSuggestions();
  }, [query, refetchSuggestions]);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(e.target.value);
    showMenu(e);
  };

  const handleSuggestionClick = (product_id: number) => {
    navigate(`/product/${product_id}`, { state: { product_id } });
    setQuery("");
  };

  const { showMenu, containerMenu } = useMenu({
    menuChild: (
      <>
        {query && suggestList && suggestList.products.length > 0 && (
          <List
            sx={{
              position: "absolute",
              width: "100%",
              backgroundColor: "#ffffff",
              zIndex: 1,
            }}
          >
            {suggestList.products.map((product) => (
              <ListItem
                key={product.id}
                onClick={() => handleSuggestionClick(product.id)}
                sx={{
                  "&:hover": { backgroundColor: theme.palette.secondary.light },
                }}
              >
                <Typography>{product.title}</Typography>
              </ListItem>
            ))}
          </List>
        )}
      </>
    ),
  });

  return (
    <>
      <AppBar position="sticky" elevation={0}>
        <Container>
          <Toolbar disableGutters sx={{ justifyContent: "space-between" }}>
            <img
              src={logo}
              alt="Logo"
              width={"50px"}
              onClick={() => handleClick()}
              style={{ minWidth: "30px", cursor: "pointer" }}
            />
            <TextField
              label="Rechercher"
              variant="outlined"
              size="small"
              value={query}
              onChange={handleInputChange}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="start">
                    <Search />
                  </InputAdornment>
                ),
              }}
              sx={{ width: isMobile ? "60%" : "50%" }}
            />
            {containerMenu}
            <Box>
              <CartButton />
              {isAuthenticated ? <AccountButton /> : <LoginButton />}
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <SecondaryTopBar />
    </>
  );
}

export default TopBar;
