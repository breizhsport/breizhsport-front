import {
  Button,
  IconButton,
  Menu,
  MenuItem,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { useAppSelector } from "../../redux/store";
import { authActions, getCurrentUser } from "../../redux/auth/authSlice";
import { AccountCircle, ExitToApp } from "@mui/icons-material";
import { useState, MouseEvent } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

const AccountButton = () => {
  const theme = useTheme();
  const [menuOpen, setMenuOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const isMd = useMediaQuery(theme.breakpoints.down("md"));

  const handleMenuOpen = (event: MouseEvent<HTMLButtonElement>) => {
    setMenuOpen(true);
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setMenuOpen(false);
    setAnchorEl(null);
  };

  const handleLogout = () => {
    dispatch(authActions.logout());
  };

  const handleOpenProfil = () => {
    navigate("/profil");
  };

  const currentUser = useAppSelector(getCurrentUser);

  return (
    <>
      {isMd ? (
        <IconButton size="medium" color="secondary" onClick={handleMenuOpen}>
          <AccountCircle />
        </IconButton>
      ) : (
        <Button
          variant="contained"
          color="secondary"
          startIcon={<AccountCircle />}
          onClick={handleMenuOpen}
        >
          {!isMd && currentUser.firstname + " " + currentUser.lastname}
        </Button>
      )}
      <Menu anchorEl={anchorEl} open={menuOpen} onClose={handleMenuClose}>
        <MenuItem onClick={handleOpenProfil}>
          <AccountCircle sx={{ marginRight: 1 }} />
          {"Mon profil"}
        </MenuItem>
        <MenuItem onClick={handleLogout}>
          <ExitToApp sx={{ marginRight: 1 }} />
          {"Se déconnecter"}
        </MenuItem>
      </Menu>
    </>
  );
};

export default AccountButton;
