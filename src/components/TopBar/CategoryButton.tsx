import { Button, useTheme } from "@mui/material";
import { CategoryButtonProps } from "../../interfaces/CategoryInterface";

const CategoryButton = ({
  category,
  onClick,
  selected,
}: CategoryButtonProps) => {
  const theme = useTheme();

  const handleClick = () => {
    onClick();
  };

  return (
    <Button
      key={category.id}
      sx={{
        fontSize: theme.typography.body1,
      }}
      variant={selected ? "contained" : "text"}
      onClick={() => handleClick()}
    >
      {category.title}
    </Button>
  );
};

export default CategoryButton;
