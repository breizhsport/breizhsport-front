import {
  Badge,
  Button,
  IconButton,
  useTheme,
  useMediaQuery,
} from "@mui/material";
import { ShoppingCart } from "@mui/icons-material";
import { useNavigate } from "react-router-dom";
import { useAppSelector } from "../../redux/store";
import { getCurrentUser } from "../../redux/auth/authSlice";
import { useLazyGetCartQuery } from "../../redux/cartService";
import UnauthorizedModal from "../UnauthorizedModal";
import { useEffect, useState } from "react";

const CartButton = () => {
  const theme = useTheme();
  const navigate = useNavigate();
  const [dialogOpen, setDialogOpen] = useState(false);
  const currentUser = useAppSelector(getCurrentUser);
  const isMd = useMediaQuery(theme.breakpoints.down("md"));

  const handleClick = () => {
    if (currentUser.id == null) {
      setDialogOpen(true);
      return;
    }
    navigate(`/cart`);
  };

  const handleCloseDialog = () => {
    setDialogOpen(false);
  };

  const [trigger, result] = useLazyGetCartQuery();

  const numberOfProductsInCart = result.data?.cart.productsNumber ?? null;

  useEffect(() => {
    if (currentUser.id) {
      trigger(currentUser.id);
    }
  }, [currentUser, trigger]);

  return (
    <>
      {isMd ? (
        <IconButton color="secondary" onClick={() => handleClick()}>
          <ShoppingCart />
        </IconButton>
      ) : (
        <Badge
          badgeContent={numberOfProductsInCart}
          color="error"
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
        >
          <Button
            color="secondary"
            variant="contained"
            onClick={() => handleClick()}
            startIcon={<ShoppingCart />}
            sx={{ marginRight: theme.spacing(1) }}
          >
            Panier
          </Button>
        </Badge>
      )}
      <UnauthorizedModal
        dialogOpen={dialogOpen}
        handleCloseDialog={handleCloseDialog}
      />
    </>
  );
};

export default CartButton;
