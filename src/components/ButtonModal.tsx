import { Box, Button, Modal, useTheme, IconButton } from "@mui/material";
import { ButtonModalParams } from "../interfaces/CoreInterface";
import { useMediaQuery } from "@mui/material";

const ButtonModal = ({
  icon,
  children,
  label,
  id,
  open,
  setOpen,
}: ButtonModalParams) => {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.down("md")); // Ajout de la notion de responsive

  const style = {
    position: "absolute" as const,
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
    borderRadius: 8,
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      {isMd ? (
        <IconButton
          size="medium"
          id={id}
          color="secondary"
          onClick={handleOpen}
        >
          {icon}
        </IconButton>
      ) : (
        <Button
          id={id}
          variant="contained"
          color="secondary"
          startIcon={icon}
          onClick={handleOpen}
          sx={{ marginRight: theme.spacing(1) }}
        >
          {label}
        </Button>
      )}
      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>{children}</Box>
      </Modal>
    </>
  );
};

export default ButtonModal;
