import {
  Card,
  Typography,
  Button,
  CardContent,
  CardActionArea,
  useTheme,
} from "@mui/material";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import { ProductCardProps } from "../../interfaces/ProductInterface";
import { useNavigate } from "react-router-dom";
import { useAddProductCartMutation } from "../../redux/cartService";
import { toast } from "react-toastify";
import { useAppSelector } from "../../redux/store";
import { getCurrentUser } from "../../redux/auth/authSlice";
import { useState } from "react";
import UnauthorizedModal from "../UnauthorizedModal";

const ProductCard = ({ product }: ProductCardProps) => {
  const navigate = useNavigate();
  const handleClick = (product_id: number) => {
    navigate(`/product/${product_id}`, {
      state: { product_id },
    });
  };

  const currentUser = useAppSelector(getCurrentUser);
  const theme = useTheme();

  const [addToCart, {}] = useAddProductCartMutation();
  const [dialogOpen, setDialogOpen] = useState(false);

  const handleClickAddProduct = (productId: number) => {
    if (currentUser.id == null) {
      setDialogOpen(true); // Ouvrir la boîte de dialogue si currentUser est null
      return;
    }
    try {
      addToCart({ productId: productId });
      toast.success("Produit ajouté au panier");
    } catch (error) {
      toast.error("Erreur lors de l'ajout au panier");
    }
  };

  const handleCloseDialog = () => {
    setDialogOpen(false); // Fermer la boîte de dialogue
  };

  return (
    <Card sx={{ backgroundColor: theme.palette.primary.light }}>
      <CardContent>
        <CardActionArea onClick={() => handleClick(product.id)}>
          <img
            src={product.image}
            alt={product.title}
            style={{
              width: "100%",
              height: "auto",
              objectFit: "cover",
              maxHeight: "200px",
              minHeight: "200px",
            }}
          />
          <Typography
            variant="h6"
            color="primary"
            paddingTop={theme.spacing(1)}
            sx={{ fontSize: "1rem", textAlign: "center" }}
            overflow="hidden"
            textOverflow="ellipsis"
            whiteSpace="nowrap"
          >
            {product.title}
          </Typography>
          <Typography
            variant="h5"
            color={theme.palette.primary.dark}
            sx={{ fontSize: "1.25rem", textAlign: "center" }}
          >
            {product.price + "€"}
          </Typography>
        </CardActionArea>
        <Button
          onClick={() => handleClickAddProduct(product.id)}
          variant="contained"
          color="secondary"
          startIcon={<AddShoppingCartIcon />}
          sx={{
            marginTop: theme.spacing(2),
            width: "100%",
          }}
        >
          Ajouter au panier
        </Button>
      </CardContent>
      <UnauthorizedModal
        dialogOpen={dialogOpen}
        handleCloseDialog={handleCloseDialog}
      />
    </Card>
  );
};

export default ProductCard;
