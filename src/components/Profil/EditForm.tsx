import { Close } from "@mui/icons-material";
import {
  Box,
  Button,
  FormControl,
  IconButton,
  TextField,
  Typography,
} from "@mui/material";

const EditForm = ({ handleClose }: { handleClose: () => void }) => {
  return (
    <FormControl margin="none" fullWidth>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Typography variant="h5">Modifier mon profil</Typography>
        <IconButton onClick={handleClose}>
          <Close />
        </IconButton>
      </Box>
      <TextField label="Adresse mail"></TextField>
      <TextField label="Mot de passe"></TextField>
      <Button variant="contained">Sauvegarder</Button>
    </FormControl>
  );
};

export default EditForm;
