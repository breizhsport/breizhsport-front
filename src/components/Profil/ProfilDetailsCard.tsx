import { Box, Card, CardContent, Typography, useTheme } from "@mui/material";
import { useAppSelector } from "../../redux/store";
import { getCurrentUser } from "../../redux/auth/authSlice";
import { useGetCustomerByUserIdQuery } from "../../redux/customerService";

const ProfilDetailsCard = () => {
  const theme = useTheme();
  const currentUser = useAppSelector(getCurrentUser);
  const { data } = useGetCustomerByUserIdQuery(currentUser.id);
  return (
    <Card>
      <CardContent>
        <Typography
          variant="h5"
          color="primary"
          marginBottom={theme.spacing(1)}
        >
          Mon profil
        </Typography>
        <Typography variant="h6">
          {data?.customer.firstname + " " + data?.customer.lastname}
        </Typography>
        <Typography variant="h6" marginBottom={theme.spacing(3)} noWrap={true}>
          {currentUser.email}
        </Typography>
        <Typography
          variant="h5"
          color="primary"
          marginBottom={theme.spacing(1)}
        >
          Mon adresse de livraison
        </Typography>
        <Typography variant="h6">{data?.customer.address}</Typography>
        <Box display="flex">
          <Typography variant="h6">
            {data?.customer.zipcode + " " + data?.customer.city}
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
};

export default ProfilDetailsCard;
