import { Edit } from "@mui/icons-material";
import ButtonModal from "../ButtonModal";
import EditForm from "./EditForm";
import { useState } from "react";

const EditProfilButton = () => {
  const [open, setOpen] = useState(false);
  const handleClose = () => setOpen(false);
  return (
    <ButtonModal
      label="Editer mon profil"
      icon={<Edit />}
      children={<EditForm handleClose={handleClose} />}
      open={open}
      setOpen={setOpen}
    />
  );
};

export default EditProfilButton;
