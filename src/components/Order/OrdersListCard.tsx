import { Alert, Typography } from "@mui/material";
import { getCurrentUser } from "../../redux/auth/authSlice";
import { useGetOrdersListQuery } from "../../redux/orderService";
import { useAppSelector } from "../../redux/store";
import OrderCard from "../Cart/OrderCard";

const OrdersListCard = () => {
  const currentUser = useAppSelector(getCurrentUser);
  const { data: ordersList } = useGetOrdersListQuery(currentUser.id);

  return (
    <>
      {ordersList?.orders.length == 0 ? (
        <Alert severity="info">
          <Typography>
            Vous n'avez pas passé de commande sur BreizhSport
          </Typography>
        </Alert>
      ) : (
        ordersList?.orders.map((order) => (
          <OrderCard key={order.id} order={order} />
        ))
      )}
    </>
  );
};

export default OrdersListCard;
