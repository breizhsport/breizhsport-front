import {
  FormControl,
  Grid,
  Typography,
  TextField,
  Button,
  useTheme,
  Box,
} from "@mui/material";

import { useForm } from "react-hook-form";
import { useState } from "react";
import PaymentConfirmationModal from "./PaymentConfirmationModal";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { PaymentFormInput } from "../../interfaces/PaymentInterface";
import { useCreateOrderMutation } from "../../redux/orderService";
import { PaymentFormProps } from "../../interfaces/CartInterface";

const PaymentForm = ({ cartId }: PaymentFormProps) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<PaymentFormInput>({
    defaultValues: {
      creditCardName: "",
      creditCardNumber: "",
      validityDate: "",
      CVC: "",
    },
  });
  const theme = useTheme();
  const navigate = useNavigate();

  const [open, setOpen] = useState(false);

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === "Enter") {
      handleSubmit(onSubmit)();
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const [createOrder, {}] = useCreateOrderMutation();

  const onSubmit = () => {
    setOpen(true);
    createOrder(cartId);
    setTimeout(() => {
      handleClose();
      navigate("/");
      toast.success("Paiement réussi !");
    }, 3000);
  };

  return (
    <Grid container justifyContent="center">
      <Grid item xs={12} md={6}>
        <Box onKeyDown={handleKeyDown}>
          <FormControl fullWidth>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Typography>Nom de la carte</Typography>
                <TextField
                  size="small"
                  placeholder="Nom du propriétaire de la carte"
                  fullWidth
                  {...register("creditCardName", {
                    required: { value: true, message: "Valeur manquante" },
                  })}
                  error={!!errors.creditCardName}
                  helperText={errors.creditCardName?.message}
                />
              </Grid>
              <Grid item xs={12}>
                <Typography>Numéro de votre carte bleue</Typography>
                <TextField
                  size="small"
                  placeholder="XXXX-XXXX-XXXX-XXXX"
                  fullWidth
                  {...register("creditCardNumber", {
                    required: { value: true, message: "Valeur manquante" },
                  })}
                  error={!!errors.creditCardNumber}
                  helperText={errors.creditCardNumber?.message}
                />
              </Grid>
              <Grid item xs={6}>
                <Typography>Date de validité</Typography>
                <TextField
                  size="small"
                  placeholder="MM/AAAA"
                  fullWidth
                  {...register("validityDate", {
                    required: { value: true, message: "Valeur manquante" },
                  })}
                  error={!!errors.validityDate}
                  helperText={errors.validityDate?.message}
                />
              </Grid>
              <Grid item xs={6}>
                <Typography>CVC</Typography>
                <TextField
                  placeholder="XXX"
                  size="small"
                  fullWidth
                  {...register("CVC", {
                    required: { value: true, message: "Valeur manquante" },
                  })}
                  error={!!errors.CVC}
                  helperText={errors.CVC?.message}
                />
              </Grid>
            </Grid>
            <Button
              variant="contained"
              color="success"
              sx={{ marginTop: theme.spacing(4) }}
              onClick={handleSubmit(onSubmit)}
            >
              Procéder au paiement
            </Button>
          </FormControl>
        </Box>
        <PaymentConfirmationModal open={open} handleClose={handleClose} />
      </Grid>
    </Grid>
  );
};

export default PaymentForm;
