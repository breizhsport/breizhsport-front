import { Dialog, Box, CircularProgress, Typography } from "@mui/material";
import { PaymentConfirmationModalParams } from "../../interfaces/PaymentInterface";

const PaymentConfirmationModal = ({
  open,
  handleClose,
}: PaymentConfirmationModalParams) => {
  return (
    <Dialog open={open} onClose={handleClose}>
      <Box p={2} textAlign="center">
        <CircularProgress />
        <Typography>Votre paiement est en cours de traitement</Typography>
      </Box>
    </Dialog>
  );
};

export default PaymentConfirmationModal;
