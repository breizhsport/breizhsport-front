import { Outlet } from "react-router-dom";
import TopBar from "./TopBar/TopBar";
import { Box, useTheme } from "@mui/material";
import FooterBase from "./Footer/FooterBase";

const Layout = () => {
  const theme = useTheme();
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        minHeight: "100vh",
        position: "sticky",
        top: 0,
      }}
    >
      <TopBar />
      <Box sx={{ margin: theme.spacing(4), flex: 1 }}>
        <Outlet />
      </Box>
      <Box sx={{ flexShrink: 0, bottom: 0 }}>
        <FooterBase />
      </Box>
    </Box>
  );
};
export default Layout;
