import { Box, Card, CardContent, Typography, useTheme } from "@mui/material";
import { CartSummaryProps } from "../../interfaces/CartInterface";

const CartSummaryCard = ({ cart }: CartSummaryProps) => {
  const theme = useTheme();
  return (
    <>
      <Card
        key={cart.id}
        sx={{ backgroundColor: theme.palette.secondary.light }}
      >
        <CardContent>
          <Typography variant="h5" color="primary">
            Récapitulatif de votre panier
          </Typography>
          <Box display="flex" justifyContent="end">
            <Box display={"flex"} flexDirection={"column"}>
              <Typography variant="body1">
                Nombre de produits : {cart.productsNumber}
              </Typography>
              <Typography variant="body1">
                Total prix de la commande : {cart.totalPrice} €
              </Typography>
            </Box>
          </Box>
        </CardContent>
      </Card>
    </>
  );
};

export default CartSummaryCard;
