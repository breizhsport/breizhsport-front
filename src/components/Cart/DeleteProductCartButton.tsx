import { Delete } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import { useDeleteProductCartMutation } from "../../redux/cartService";
import { toast } from "react-toastify";
import { DeleteProductCartInput } from "../../interfaces/CartInterface";

const DeleteProductCartButton = ({ productId }: DeleteProductCartInput) => {
  const [deleteProductToCart] = useDeleteProductCartMutation();

  const handleClickDeleteProduct = (productIdDeleted: number) => {
    try {
      deleteProductToCart({ productId: productIdDeleted });
      toast.success("Produit supprimé du panier");
    } catch (error) {
      toast.error("Erreur lors de la suppression du produit du panier");
    }
  };

  return (
    <>
      <IconButton onClick={() => handleClickDeleteProduct(productId)}>
        <Delete />
      </IconButton>
    </>
  );
};

export default DeleteProductCartButton;
