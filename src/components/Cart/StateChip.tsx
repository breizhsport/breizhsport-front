import { Chip } from "@mui/material";

const StateChip = () => {
  return (
    <>
      <Chip color="success" label="Livrée" />
    </>
  );
};

export default StateChip;
