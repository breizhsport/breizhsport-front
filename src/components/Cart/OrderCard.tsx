import {
  Button,
  Card,
  CardContent,
  Typography,
  useTheme,
  Box,
  Collapse,
} from "@mui/material";
import { OrderCardParams } from "../../interfaces/OrderInterface";
import StateChip from "./StateChip";
import { useState } from "react";
import { ArrowDropDown, ArrowDropUp } from "@mui/icons-material";

const OrderCard = ({ order }: OrderCardParams) => {
  const theme = useTheme();
  const [expand, setExpand] = useState(false);

  const createdDateOrder = new Intl.DateTimeFormat("fr-FR", {
    year: "numeric",
    month: "numeric",
    day: "numeric",
  }).format(order.createdAt);

  return (
    <>
      <Card
        sx={{
          backgroundColor: theme.palette.secondary.light,
          marginBottom: theme.spacing(2),
        }}
      >
        <CardContent>
          <Box display="flex">
            <Typography variant="h5" color="primary">
              {`Commande n°${order.id} ⎟ `}
            </Typography>
            <StateChip />
          </Box>
          <Typography>Date : {createdDateOrder}</Typography>
          <Typography>Montant : {order.totalPrice} €</Typography>
          <Typography marginBottom={theme.spacing(1)}>
            Nombre de produits : {order.productsNumber}
          </Typography>
          <Button
            variant="contained"
            onClick={() => setExpand(!expand)}
            sx={{ marginBottom: theme.spacing(2) }}
            endIcon={expand ? <ArrowDropUp /> : <ArrowDropDown />}
          >
            Voir le détail
          </Button>
          <Collapse in={expand}>
            {order.products.map((product) => (
              <Card key={product.id} elevation={0}>
                <CardContent>
                  <Typography>{product.title}</Typography>
                  <Typography>{`Quantité : ${product.quantity}`}</Typography>
                  <Typography>{`Prix unitaire : ${product.price} €`}</Typography>
                </CardContent>
              </Card>
            ))}
          </Collapse>
        </CardContent>
      </Card>
    </>
  );
};

export default OrderCard;
