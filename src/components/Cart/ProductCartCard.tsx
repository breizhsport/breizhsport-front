import {
  Card,
  CardContent,
  Stack,
  Typography,
  Box,
  useTheme,
} from "@mui/material";
import UpdateQuantityButton from "./UpdateQuantityButton";
import { useState } from "react";
import DeleteProductCartButton from "./DeleteProductCartButton";
import { ProductCartCardProps } from "../../interfaces/CartInterface";

const ProductCartCard = ({ product }: ProductCartCardProps) => {
  const theme = useTheme();
  const [quantity, setQuantity] = useState<number>(product.quantity);
  return (
    <>
      <Card
        key={product.product_id}
        sx={{
          backgroundColor: theme.palette.primary.light,
          marginBottom: theme.spacing(2),
        }}
      >
        <CardContent>
          <Typography variant="h6" color="primary">
            {product.title}
          </Typography>
          <Typography variant="body1">
            Prix unitaire : {product.price} €
          </Typography>
          <Box display="flex" justifyContent="end">
            <Stack direction="row" alignItems="center">
              <UpdateQuantityButton
                product={product}
                setQuantity={setQuantity}
                quantity={quantity}
              />
            </Stack>
            <DeleteProductCartButton productId={product.product_id} />
          </Box>
        </CardContent>
      </Card>
    </>
  );
};

export default ProductCartCard;
