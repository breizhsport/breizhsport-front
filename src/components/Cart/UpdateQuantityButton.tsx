import { useCallback } from "react";
import { Add, Remove } from "@mui/icons-material";
import { IconButton, Stack, Typography } from "@mui/material";
import { UpdateQuantityButtonProps } from "../../interfaces/CartInterface";
import {
  useGetCartQuery,
  useUpdateQuantityMutation,
} from "../../redux/cartService";
import { useAppSelector } from "../../redux/store";
import { getCurrentUser } from "../../redux/auth/authSlice";

const UpdateQuantityButton = ({
  product,
  setQuantity,
  quantity,
}: UpdateQuantityButtonProps) => {
  const [updateQuantity, {}] = useUpdateQuantityMutation();
  const currentUser = useAppSelector(getCurrentUser);
  const { data } = useGetCartQuery(currentUser?.id);

  const handleClick = (newQuantity: number) => {
    updateQuantity({
      productId: product.product_id,
      cartId: data?.cart.id,
      userId: currentUser.id,
      newQuantity: newQuantity,
    });
  };
  const handleIncrement = useCallback(() => {
    setQuantity((prevQuantity: number) => {
      const newQuantity = prevQuantity + 1;
      handleClick(newQuantity);
      return newQuantity;
    });
  }, [setQuantity, handleClick]);

  const handleDecrement = useCallback(() => {
    setQuantity((prevQuantity: number) => {
      const newQuantity = Math.max(prevQuantity - 1, 1);
      handleClick(newQuantity);
      return newQuantity;
    });
  }, [setQuantity, handleClick]);

  return (
    <>
      <Stack direction="row" alignItems="center">
        <IconButton
          color="primary"
          size="small"
          sx={{
            p: "4px",
            borderRadius: "50%",
            mr: 0.5,
          }}
          onClick={handleDecrement}
        >
          <Remove fontSize="small" />
        </IconButton>

        <Typography variant="body1">{quantity}</Typography>

        <IconButton
          color="primary"
          size="small"
          sx={{
            p: "4px",
            borderRadius: "50%",
            mr: 0.5,
          }}
          onClick={handleIncrement}
        >
          <Add fontSize="small" />
        </IconButton>
      </Stack>
    </>
  );
};

export default UpdateQuantityButton;
