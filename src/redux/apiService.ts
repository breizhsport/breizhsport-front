import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { API_BASE_URL } from "../config/config";
import { RootState } from "./store";
import { getAccessTokenUser } from "./auth/authSlice";

export const breizhsportApi = createApi({
  reducerPath: "breizhsportApi",
  baseQuery: fetchBaseQuery({
    baseUrl: API_BASE_URL,
    prepareHeaders: (headers, { getState }) => {
      const token = getAccessTokenUser(getState() as RootState);
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: () => ({}),
  tagTypes: ["cart", "products", "users", "orders", "category"],
});
