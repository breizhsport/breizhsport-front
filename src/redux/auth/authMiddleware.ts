import { Middleware } from "@reduxjs/toolkit";
import { LoginService } from "../userService";
import { authActions } from "./authSlice";

export const authMiddleware: Middleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    if (LoginService.endpoints.login.matchFulfilled(action)) {
      dispatch(authActions.login(action.payload));
    }
    // if (action.error) {
    //   dispatch(authActions.logout());
    // }
    return next(action);
  };
