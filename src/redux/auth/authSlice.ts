import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { LoginResponse, User } from "../../interfaces/UserInterface";
import { RootState } from "../store";

interface AuthState {
  user: User;
}

const initialState: AuthState = {
  user: JSON.parse(localStorage.getItem("user") ?? "{}"),
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    login: (state: AuthState, action: PayloadAction<LoginResponse>) => {
      state.user = action.payload.user;
      localStorage.setItem("user", JSON.stringify(action.payload.user));
    },
    logout: (state: AuthState) => {
      state.user = {} as User;
      localStorage.removeItem("user");
    },
  },
});

export const getCurrentUser = (state: RootState) => state.auth.user;
export const getAccessTokenUser = (state: RootState) => state.auth.user.token;

export const authActions = authSlice.actions;
export default authSlice.reducer;
