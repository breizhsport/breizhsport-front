import {
  AddProductCartInput,
  DeleteProductCartInput,
  GetCartResponse,
  UpdateQuantityInput,
} from "../interfaces/CartInterface";
import { breizhsportApi } from "./apiService";

export const cartService = breizhsportApi.injectEndpoints({
  endpoints: (builder) => ({
    getCart: builder.query<GetCartResponse, number>({
      query: () => ({
        url: `/api/cart/`,
        method: "GET",
      }),
      providesTags: ["cart"],
    }),
    addProductCart: builder.mutation<void, AddProductCartInput>({
      query: (data) => ({
        url: `/api/cart/product/${data.productId}`,
        method: "POST",
      }),
      invalidatesTags: ["cart"],
    }),
    updateQuantity: builder.mutation<void, UpdateQuantityInput>({
      query: (data) => ({
        url: `/api/cart/product/${data.productId}`,
        method: "PATCH",
        body: {
          quantity: data.newQuantity,
        },
      }),
      invalidatesTags: ["cart"],
    }),
    deleteProductCart: builder.mutation<void, DeleteProductCartInput>({
      query: (data) => ({
        url: `api/cart/product/${data.productId}`,
        method: "DELETE",
      }),
      invalidatesTags: ["cart"],
    }),
  }),
});

export const {
  useGetCartQuery,
  useLazyGetCartQuery,
  useDeleteProductCartMutation,
  useAddProductCartMutation,
  useUpdateQuantityMutation,
} = cartService;
