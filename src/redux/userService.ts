import {
  LoginResponse,
  RegisterFormInput,
  User,
} from "../interfaces/UserInterface";
import { breizhsportApi } from "./apiService";

export const LoginService = breizhsportApi.injectEndpoints({
  endpoints: (builder) => ({
    login: builder.mutation<LoginResponse, FormData>({
      query: (loginCredentials) => ({
        url: "/api/login",
        method: "POST",
        body: loginCredentials,
        formData: true,
      }),
    }),
    signIn: builder.mutation<void, RegisterFormInput>({
      query: (args) => ({
        url: "/api/register",
        method: "POST",
        body: args,
      }),
    }),
    getUser: builder.query<User, number>({
      query: (userId) => ({
        url: `/api/customer${userId}`,
        method: "GET",
        body: userId,
      }),
    }),
  }),
});

export const { useLoginMutation, useSignInMutation, useGetUserQuery } =
  LoginService;
