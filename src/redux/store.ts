import { configureStore } from "@reduxjs/toolkit";
import { breizhsportApi } from "./apiService";
import { authMiddleware } from "./auth/authMiddleware";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import authSlice from "./auth/authSlice";

export const store = configureStore({
  reducer: {
    breizhsportApi: breizhsportApi.reducer,
    auth: authSlice,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      breizhsportApi.middleware, 
      authMiddleware
    ),
})

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;