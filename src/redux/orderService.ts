import { OrdersListResponse } from "../interfaces/OrderInterface";
import { breizhsportApi } from "./apiService";

export const OrderService = breizhsportApi.injectEndpoints({
  endpoints: (builder) => ({
    getOrdersList: builder.query<OrdersListResponse, number>({
      query: () => ({
        url: `/api/orders`,
        method: "GET",
      }),
      providesTags: ["orders"],
    }),
    createOrder: builder.mutation<void, number>({
      query: (cartId) => ({
        url: `/api/order`,
        method: "POST",
        params: {
          cartId,
        },
      }),
      invalidatesTags: ["orders", "cart"],
    }),
  }),
});

export const { useGetOrdersListQuery, useCreateOrderMutation } = OrderService;
