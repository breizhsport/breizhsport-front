import { CategoriesListResponse } from "../interfaces/CategoryInterface";
import { breizhsportApi } from "./apiService";

export const categoryService = breizhsportApi.injectEndpoints({
  endpoints: (builder) => ({
    getCategoriesList: builder.query<CategoriesListResponse, void>({
      query: () => ({
        url: "/api/categories",
        method: "GET",
      }),
    }),
  }),
});

export const { useGetCategoriesListQuery } = categoryService;
