import { GetCustomerResponse } from "../interfaces/CustomerInterface";
import { breizhsportApi } from "./apiService";

export const customerService = breizhsportApi.injectEndpoints({
  endpoints: (builder) => ({
    getCustomerByUserId: builder.query<GetCustomerResponse, number>({
      query: (userId) => ({
        url: `/api/customer?user_id=${userId}`,
        method: "GET",
      }),
    }),
  }),
});

export const { useGetCustomerByUserIdQuery } = customerService;
