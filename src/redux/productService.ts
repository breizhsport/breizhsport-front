import {
  ProductListResponse,
  ProductResponse,
} from "../interfaces/ProductInterface";
import { breizhsportApi } from "./apiService";

export const ProductService = breizhsportApi.injectEndpoints({
  endpoints: (builder) => ({
    getProductsList: builder.query<ProductListResponse, number>({
      query: (categoryId) => ({
        url: `/api/category/${categoryId}/products`,
        method: "GET",
      }),
      providesTags: ["products"],
    }),
    getRecentProductsList: builder.query<ProductListResponse, number>({
      query: (qty) => ({
        url: `/api/products`,
        method: "GET",
        params: {
          qty: qty,
        },
      }),
      providesTags: ["products"],
    }),
    getProduct: builder.query<ProductResponse, number>({
      query: (product_id) => ({
        url: `/api/product/${product_id}`,
        methode: "GET",
      }),
    }),
    getSuggestions: builder.query<ProductListResponse, string>({
      query: (productNameSuggest) => ({
        url: `/api/suggest`,
        methode: "GET",
        params: {
          query: productNameSuggest,
        },
      }),
    }),
  }),
});

export const {
  useGetProductsListQuery,
  useLazyGetProductsListQuery,
  useLazyGetProductQuery,
  useGetProductQuery,
  useGetRecentProductsListQuery,
  useGetSuggestionsQuery,
} = ProductService;
