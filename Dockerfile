# Utilisez l'image de base Node.js
FROM node:20.2-alpine

# Définir le répertoire de travail
WORKDIR /app

COPY package.json .
RUN npm i
COPY . .

# Exposer le port 3000
EXPOSE 5173

# Démarrer l'application React
CMD ["npm", "run", "dev"]