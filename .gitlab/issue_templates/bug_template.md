# Rapport de Bug - [Nom du Bug]

## Description
[Description détaillée du bug, y compris les étapes pour reproduire le problème.]

## Comportement Attendu
[Décrivez le comportement attendu du système avant que le bug ne se produise.]

## Comportement Actuel
[Décrivez le comportement actuel observé, indiquant clairement le problème.]

## Environnement
- **Version de GitLab**: [Version de GitLab où le bug a été observé, par exemple, 14.2.1]
- **Navigateur (le cas échéant)**: [Nom et version du navigateur, si applicable]
- **Système d'exploitation**: [Nom et version du système d'exploitation]

## Étapes de Reproduction
1. [Étape 1]
2. [Étape 2]
3. [Étape 3]
   ...

## Captures d'écran
[Ajoutez des captures d'écran pour illustrer le problème, si possible.]

## Logs et Messages d'Erreur
[Ajoutez tout message d'erreur ou journal pertinent lié au bug.]

## Priorité
[Attribuez une priorité au bug (par exemple, haute, moyenne, basse) en fonction de son impact.]

## Impact
[Décrivez l'impact du bug sur l'utilisateur ou le système.]

## Informations Supplémentaires
[Ajoutez ici toute information supplémentaire, liens vers des discussions antérieures, etc.]

## Responsables
- [Nom du rapporteur du bug]